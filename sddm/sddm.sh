#!/bin/bash
# Customized environment (LANG definition):
if [ -f /etc/default/sddm ]; then
  . /etc/default/sddm
fi
/usr/bin/sddm.bin "$*"
