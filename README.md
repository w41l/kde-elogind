# INFO

Just some additional packages to be used in KDE/PLASMA 5

# NOTE

Please read the README file if provided in package directory.

# kwinft for PLASMA 5.19 beta (5.18.90)

These packages are used as an alternative for kwin and intended
to be used in wayland session. I found out that kwinft is more
stable and less buggy than kwin in wayland.

kwinft build order, please read $PKG/README if provided:

* wrapland
* kwinft
* disman
* kdisplay
